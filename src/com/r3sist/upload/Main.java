package com.r3sist.upload;

import com.r3sist.upload.model.Account;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.List;

public class Main {

    private final static String HEADER = "UPLOAD & R3SIST v0.1";
    private final static String BASE_PATH = "/home/alex/"; //TODO: To be changed according to the local path of the .jar
    private final static String CONFIG_FILE_NAME = "config.txt";
    private final static String SUBS_DIRECTORY_NAME = "subs/";

    private static List<Account> accountList;

    public static void main(String[] args) {

        System.out.println("\n");

        accountList = new ArrayList<>();

        //Setting the header
        prepareGUI(HEADER);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Parsing the config file
        System.out.println("\nGoing to parse account information from file: " + CONFIG_FILE_NAME + "\n");

        parseConfigFile();

        System.out.println("\nRedirecting to main menu in 2s.");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Loading the menu
        createMenu();

    }

    /**
     * Creates the main menu
     */
    private static void createMenu(){

        Account selectedAccount;

        System.out.println("\n====================\n MAIN MENU\n====================");
        int index = 0;

        //Loop for the menu entries
        for (Account account : accountList) {
            System.out.println(index + 1 + ". Upload sub to " + account.getDomain());
            index++;
        }
        //Waiting for user input
        //TODO: Handle exceptions for wrong inputs
        System.out.println("\nChoose an option or type 0 to exit: ");
        Scanner keyboardInput = new Scanner(System.in);
        int selectedDomainIndex = 0;
        try {
            selectedDomainIndex = keyboardInput.nextInt();
            if (selectedDomainIndex == 0){
                System.out.println("\n\nExiting... Viv3 La R3sist3nc3!");
                System.exit(0);
            } else if (selectedDomainIndex > accountList.size() || selectedDomainIndex < 0){
                System.out.println("\nWrong input. Restarting...");
                createMenu();
            } else {
                selectedAccount = accountList.get(selectedDomainIndex - 1);
                System.out.println("\nYour sub will be uploaded to: " + selectedAccount.getDomain() + ", please wait...");

                //Start upload to the selected website
                initiateUpload(selectedAccount.getDomain());
            }
        }catch (InputMismatchException ime){
            System.out.println("\nOnly numbers are allowed. Restarting...");
            createMenu();
        }
    }

    /**
     * Parse the config files containing the account informations
     */
    private static void parseConfigFile() {

        try {
            //Scanning the whole file
            Scanner scanner = new Scanner(new File(BASE_PATH + CONFIG_FILE_NAME));
            scanner.useDelimiter("\\A");
            String fullFile = scanner.next();

            //Splitting the file at each line
            String[] textArray = fullFile.split("\\n");
            String[] accountDetails;

            //Looping the array and splitting the strings, in order to get the account information for a website
            //STRING PATTERN:
            //{baseURL};{username};{password}
            for (int i = 0; i < textArray.length; i++){
                if (textArray[i].charAt(0) != '*'){
                    accountDetails = textArray[i].split(";");
                    Account account = new Account(accountDetails[0], accountDetails[1], accountDetails[2]);
                    System.out.println(account.toString());
                    accountList.add(account);
                }
            }

            System.out.println("\nSuccessfully parsed " + accountList.size() + " account(s).");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Sets the header of the CLI
     * @param header Header of the CLI
     */
    private static void prepareGUI(String header){

        //Setting the image
        BufferedImage bufferedImage = new BufferedImage(
                200, 50,
                BufferedImage.TYPE_INT_RGB);
        Graphics graphics = bufferedImage.getGraphics();

        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics2D.drawString(header, 12, 24);

        //Drawing the string
        for (int y = 0; y < 50; y++) {
            StringBuilder stringBuilder = new StringBuilder();

            for (int x = 0; x < 200; x++) {
                stringBuilder.append(bufferedImage.getRGB(x, y) == -16777216 ? " " : "+");
            }

            if (stringBuilder.toString().trim().isEmpty()) {
                continue;
            }

            System.out.println(stringBuilder);
        }
    }

    /**
     *Upload the sub to Podnapisi
     */
    private static void uploadToPodnapisi(String[] fileList, String fileName) {
        boolean batchUpload = false;
        if (fileList != null) {
            batchUpload = true;
        }
        if (batchUpload){
            //TODO: Iterate the array and send POST for each sub
        }else {
            //TODO: Send post for the @param fileName
        }

    }

    /**
     *Upload the sub to Subscene
     */
    private static void uploadToSubscene(String[] fileList, String fileName) {
        Account subsceneAccount = null;
        for (Account account : accountList){
            if (account.getDomain().contains("subscene.com")){
                subsceneAccount = account;
            }
        }
        loginToSubcene(subsceneAccount.getUsername(), subsceneAccount.getPassword());
        boolean batchUpload = false;
        if (fileList != null) {
            batchUpload = true;
        }
        if (batchUpload){
            //TODO: Iterate the array and send POST for each sub
        }else {
            //TODO: Send post for the @param fileName
        }
    }

    /**
     * Login to Subscene in order to get session cookies
     * @param username Subscene username
     * @param password Subscene password
     */
    private static void loginToSubcene(String username, String password) {
        /*CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet getCall = new HttpGet("https://subscene.com/account/login");
        System.out.println("\nLogging to Subscene in order to get session cookies...");
        try {
            CloseableHttpResponse response = httpClient.execute(getCall);
            System.out.println("\n[SUBSCENE LOGIN]First hop.");
            HttpPost postCall = new HttpPost("https://identity.jeded.com/core/login?signin=3c800c795c8b5df891df62667d152c70");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", "R3sist"));
            params.add(new BasicNameValuePair("password", "italiansubs"));
            postCall.setHeaders(response.getHeaders("Set-Cookie"));
            postCall.setEntity(new UrlEncodedFormEntity(params));
            response = httpClient.execute(postCall);

            System.out.println("\n[SUBSCENE LOGIN]Result: " + response.getStatusLine().getStatusCode());
            httpClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        CloseableHttpClient httpclient = HttpClients.custom()
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
        HttpClientContext context = HttpClientContext.create();
        HttpGet httpGet = new HttpGet("https://subscene.com/account/login");
        System.out.println("Executing request " + httpGet.getRequestLine());
        System.out.println("----------------------------------------");
        try {
            CloseableHttpResponse getResponse = httpclient.execute(httpGet, context);
            List<URI> redirectLocations = context.getRedirectLocations();
            List<Cookie> cookies = context.getCookieStore().getCookies();
            for (Cookie cookie : cookies){
                System.out.println("[SERVER-COOKIES] Cookie: " + cookie.toString());
            }
            System.out.println("\n[SERVER] Going to POST...");
            HttpPost postCall = new HttpPost(redirectLocations.get(1));
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", "R3sist"));
            params.add(new BasicNameValuePair("password", "italiansubs"));
            postCall.setEntity(new UrlEncodedFormEntity(params));
            /*for (Cookie cookie : cookies){
                if (cookie.getName().equals("__cfduid")){
                    String cookieString = cookie.getName() + "=" + cookie.getValue() +"; expires="
                            + cookie.getExpiryDate() + "; path=" + cookie.getPath() + "; domain=" + cookie.getDomain()
                            + ";";
                    System.out.println("[COOKIEFACTORY] Cookie string: " + cookieString);
                    postCall.addHeader("Cookie", cookieString);
                }
            }*/
            CloseableHttpResponse postResponse = httpclient.execute(postCall, context);
            System.out.println("[SERVER] Server response: " + postResponse.getStatusLine().getStatusCode());

            httpGet = new HttpGet("https://u.subscene.com/upload/?Title=the+last+ship");
            httpclient.execute(httpGet, context);
            httpclient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *Upload the sub to Addicted
     */
    private static void uploadToAddicted(String[] fileList, String fileName) {
        boolean batchUpload = false;
        if (fileList != null) {
            batchUpload = true;
        }
        if (batchUpload){
            //TODO: Iterate the array and send POST for each sub
        }else {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost postCall = new HttpPost("http://pipodi.duckdns.org/split");
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addBinaryBody("srtData", new File(BASE_PATH + SUBS_DIRECTORY_NAME + fileName),
                    ContentType.APPLICATION_OCTET_STREAM, fileName);
            builder.addTextBody("translators", "1");
            HttpEntity multipart = builder.build();
            postCall.setEntity(multipart);
            postCall.setHeader("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0");
            try {
                System.out.println("\nGoing to POST this file (" + BASE_PATH + SUBS_DIRECTORY_NAME + fileName + ")...");
                CloseableHttpResponse response = httpClient.execute(postCall);
                if (response.getStatusLine().getStatusCode() == 200){
                    System.out.println("\n[SERVER] File successfully uploaded!");
                } else {
                    System.out.println("\n[SERVER] Something went wrong during the upload. Error code: "
                            + response.getStatusLine().getStatusCode() + " with message: " + response.getStatusLine().getReasonPhrase());
                }
                httpClient.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void initiateUpload(String domain){
        System.out.println("\nYou are about to upload your sub(s) to: " + domain);
        System.out.println("\nGetting .srt file(s) from default subs directory (path: " + BASE_PATH + SUBS_DIRECTORY_NAME + ")...");

        //Start getting file list from the directory
        File subDirectory = new File(BASE_PATH + SUBS_DIRECTORY_NAME);
        String[] filesInDir = subDirectory.list();
        Arrays.sort(filesInDir);

        List<String> srtFiles = new ArrayList<>();

        //Printing the list
        int index = 0;
        System.out.println("\n.srt files in subs folder:");
        for(String filename : filesInDir){
            //Check file extension
            if (filename.substring(filename.length() - 4, filename.length()).equals(".srt")){
                System.out.println(index + 1 +". " + filename);
                srtFiles.add(filename);
                index++;
            }
        }

        System.out.println("\nDo you wish to upload all the files to " + domain + "? (Y/N)");
        Scanner keyboardInput = new Scanner(System.in);
        String batchUpload = "";
        try {
            batchUpload = keyboardInput.nextLine();
            if (batchUpload.toUpperCase().equals("Y")){
                System.out.println("\nUploading " + srtFiles.size() + " file(s) to " + domain);
                if (domain.contains("addic7ed.com/")){
                    uploadToAddicted(filesInDir, null);
                } else if (domain.contains("subscene.com/")){
                    uploadToSubscene(filesInDir, null);
                } else if (domain.contains("podnapisi.net/")){
                    uploadToPodnapisi(filesInDir, null);
                }
            } else if (batchUpload.toUpperCase().equals("N")){
                System.out.println("\nSelect which file you wish to upload: ");
                int selectedSubIndex = 0;
                try {
                    selectedSubIndex = keyboardInput.nextInt();
                    System.out.println("\nSelected index: "+ selectedSubIndex);
                    if (selectedSubIndex > srtFiles.size() || selectedSubIndex < 0){
                        System.out.println("\nWrong input. Restarting...");
                        initiateUpload(domain);
                    } else {
                        String selectedSub = srtFiles.get(selectedSubIndex - 1);
                        System.out.println("\nGoing to upload " + selectedSub + " to " + domain);
                        if (domain.contains("addic7ed.com/")) {
                            uploadToAddicted(null, selectedSub);
                        } else if (domain.contains("subscene.com/")) {
                            uploadToSubscene(null, selectedSub);
                        } else if (domain.contains("podnapisi.net/")) {
                            uploadToPodnapisi(null, selectedSub);
                        }
                    }
                }catch (InputMismatchException ime){
                    System.out.println("\nOnly numbers are allowed. Restarting...");
                    initiateUpload(domain);
                }
            } else {
                throw new InputMismatchException();
            }
        }catch (InputMismatchException ime){
            System.out.println("\nOnly Y or N allowed. Restarting...");
            initiateUpload(domain);
        }
    }
}
