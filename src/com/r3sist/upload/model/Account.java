package com.r3sist.upload.model;

public class Account {

    private String domain;
    private String username;
    private String password;

    /**
     * Account of a single sub hosting website
     * @param domain URL of the website
     * @param username Username for the login
     * @param password Password for the login
     */

    public Account(String domain, String username, String password){
        this.domain = domain;
        this.username = username;
        this.password = password;
    }

    /**
     * Returns the URL of the website
     * @return URL of the website
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Returns the password of the account for the specific website
     * @return Password of the website
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Returns the username of the account for the specific website
     * @return Username of the website
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets a different domain for this account
     * @param domain New domain for the account
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * Sets a different password for this account
     * @param password New password for the account
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets a different username for this account
     * @param username New username for the account
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * In-line representation of the current account
     * @return String containing account information
     */
    @Override
    public String toString() {
        return "[ACCOUNT] Domain: " + this.domain + " | Username: " + this.username + " | Password: " + this.password;
    }
}
